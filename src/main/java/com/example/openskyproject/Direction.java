package com.example.openskyproject;

/**
 * Enum to select the direction of flight
 *
 * @author MS and FO
 * @version 1.0
 * @since 2022-02-09
 */
public enum Direction {

    ARRIVALS, DEPARTURES
}
