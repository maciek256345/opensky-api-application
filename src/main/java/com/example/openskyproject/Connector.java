package com.example.openskyproject;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Connector class contains the method responsible
 * for obtaining the API response, returning the
 * number of aircraft, flights for the airport
 * and airplane flights based on the response
 * obtained.
 *
 * @author MS and FO
 * @version 1.0
 * @since 2022-02-09
 */
public class Connector {


    /**
     * The method responsible for establishing
     * a connection with the API and returning the response.
     *
     * @param url - url adress
     * @return response - recieved response
     */
    public static StringBuffer getResponse(String url) {

        StringBuffer response = new StringBuffer();

        try {
            URL obj = new URL(url);
            HttpURLConnection connection =
                    (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(connection.getInputStream()));

            String line;

            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }

    /**
     * The method responsible for returning the
     * number of planes from the obtained response.
     *
     * @param response - response from getResponse()
     * @return planesAmount - amount of planes in given space
     */
    public static int getPlanesNumber(StringBuffer response) {
        int planesAmount = 0;
        JsonElement jsonTree = JsonParser.parseString(String.valueOf(response));
        if (jsonTree.isJsonObject()) {
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            JsonElement states = jsonObject.get("states");
            JsonArray array = states.getAsJsonArray();
            planesAmount = array.size();
        }
        return planesAmount;
    }


    /**
     * The method responsible for returning flights
     * (arrivals or departures) for a given airport
     * in a given time period
     *
     * @param response  - response from getResponse()
     * @param direction - arrival/departure
     * @return flights - list of flights
     */
    public static ArrayList<Flight> getAirportFlights(StringBuffer response, Direction direction) {
        ArrayList<Flight> flights = new ArrayList<>();
        JsonElement jsonTree = JsonParser.parseString(String.valueOf(response));
        JsonArray array = jsonTree.getAsJsonArray();
        for (int i = 0; i < array.size(); i++) {
            JsonObject line = array.get(i).getAsJsonObject();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Map map = gson.fromJson(line.toString(), Map.class);

            String icao24 = map.get("icao24").toString();

            String airport;
            if (direction.equals(Direction.ARRIVALS)) {
                try {
                    airport = map.get("estDepartureAirport").toString();
                } catch (NullPointerException e) {
                    airport = "Null";
                }
            } else {
                try {
                    airport = map.get("estArrivalAirport").toString();
                } catch (NullPointerException e) {
                    airport = "Null";
                }

            }
            JsonElement date;
            if (direction.equals(Direction.ARRIVALS)) {
                date = line.get("lastSeen");
            } else {
                date = line.get("firstSeen");
            }

            String dateString = date.getAsString();

            long dateLong = Long.parseLong(dateString);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date2 = sdf.format(new Date(dateLong * 1000));

            flights.add(new Flight(icao24, airport, date2));

        }

        return flights;
    }

    /**
     * The method responsible for returning flights
     * of a given aircraft in a given time period.
     *
     * @param response - response from getResponse()
     * @return planeFlights - all flights given plane
     */
    public static ArrayList<PlaneFlights> getPlanesFlights(StringBuffer response) {
        ArrayList<PlaneFlights> planeFlights = new ArrayList<>();

        JsonElement jsonTree = JsonParser.parseString(String.valueOf(response));
        JsonArray array = jsonTree.getAsJsonArray();
        for (int i = 0; i < array.size(); i++) {
            JsonObject line = array.get(i).getAsJsonObject();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Map map = gson.fromJson(line.toString(), Map.class);

            String departureAirport = "";
            String arrivalAirport = "";

            try {
                departureAirport = map.get("estDepartureAirport").toString();
            } catch (NullPointerException e) {
                departureAirport = "Null";
            }

            try {
                arrivalAirport = map.get("estArrivalAirport").toString();
            } catch (NullPointerException e) {
                arrivalAirport = "Null";
            }

            JsonElement optionalAirports = line.get("arrivalAirportCandidatesCount");
            String optionalAirportsString = optionalAirports.getAsString();

            JsonElement departureDate = line.get("firstSeen");
            JsonElement arrivalDate = line.get("lastSeen");
            String departureDateString = departureDate.getAsString();
            String arrivalDateString = arrivalDate.getAsString();

            long departureDateLong = Long.parseLong(departureDateString);
            long arrivalDateLong = Long.parseLong(arrivalDateString);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            String correctDepartureDate = sdf.format(new Date(departureDateLong * 1000));
            String correctArrivaleDate = sdf.format(new Date(arrivalDateLong * 1000));

            planeFlights.add(new PlaneFlights(departureAirport, arrivalAirport,
                    correctDepartureDate, correctArrivaleDate, optionalAirportsString));

        }


        return planeFlights;
    }

}
